﻿CREATE TABLE Contacts (
	id INT PRIMARY KEY IDENTITY,
	name VARCHAR(255),
	email VARCHAR(255),
	address VARCHAR(255),
	city VARCHAR(255),
	state VARCHAR(255),
	country VARCHAR(255),
	phone VARCHAR(255),
);