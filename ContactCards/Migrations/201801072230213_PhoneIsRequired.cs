namespace ContactCards.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhoneIsRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contacts", "Phone", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contacts", "Phone", c => c.String());
        }
    }
}
