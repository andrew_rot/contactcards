namespace ContactCards.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        DateSent = c.DateTime(nullable: false),
                        SenderID = c.String(maxLength: 128),
                        ContactID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.SenderID)
                .Index(t => t.SenderID)
                .Index(t => t.ContactID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Emails", "SenderID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Emails", "ContactID", "dbo.Contacts");
            DropIndex("dbo.Emails", new[] { "ContactID" });
            DropIndex("dbo.Emails", new[] { "SenderID" });
            DropTable("dbo.Emails");
        }
    }
}
