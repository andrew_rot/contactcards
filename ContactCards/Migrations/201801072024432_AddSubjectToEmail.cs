namespace ContactCards.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubjectToEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Emails", "Subject", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Emails", "Subject");
        }
    }
}
