﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace ContactCards.Models {
    public class Email {

        [Key]
        public virtual int Id { get; protected set; }

        [Required]
        public virtual string Subject { get; set; }

        [Display(Name = "Body")]
        public virtual string Text { get; set; }

        [Required]
        [Display(Name = "Date Sent")]
        public DateTime DateSent { get; set; }

        public virtual string SenderID { get; set; }
        public virtual ApplicationUser Sender { get; set; }

        public virtual int ContactID { get; set; }
        public virtual Contact Contact { get; set; }

        public Email() { }

        [AsyncTimeout(150)]
        [HandleError(ExceptionType = typeof(TimeoutException),
                                    View = "TimeoutError")]
        public async Task Send() {
            ApplicationDbContext db = ApplicationDbContext.Create();

            Sender = db.Users.Find(SenderID);
            Contact = db.Contacts.Find(ContactID);

            var client = new SendGridClient("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");
            var from = new EmailAddress(Sender.Email, Sender.UserName);
            var to = new EmailAddress(Contact.Email, Contact.Name);
            var msg = MailHelper.CreateSingleEmail(from, to, Subject, Text, "");
            var response = await client.SendEmailAsync(msg);
        }
    }
}