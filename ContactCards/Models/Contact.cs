﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.OleDb;
using System.Web;

namespace ContactCards.Models {
    public class Contact {
        [Key]
        public int Id { get; protected set; }

        [Required]
        public virtual string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        public virtual string Email { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        [Required]
        public virtual string Phone { get; set; }

        [Required]
        public string OwnerID { get; set; }

        public virtual ApplicationUser Owner { get; set; }

        public virtual ICollection<TextMessage> TextMessages { get; set; }

        public virtual ICollection<Email> Emails { get; set; }

        public Contact() { }

    }
}