﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace ContactCards.Models {
    public class TextMessage {
        public virtual int Id { get; set; }

        [Display(Name = "Message")]
        public virtual string Text { get; set; }

        [Display(Name = "Date Sent")]
        public virtual DateTime TimeSent { get; set; }

        [Required]
        public virtual int ContactID { get; set; }

        [Required]
        public virtual string SenderID { get; set; }

        public Contact Contact { get; set; }

        public ApplicationUser Sender { get; set; }

        public TextMessage() { }

        public void SendMessage() {
            ApplicationDbContext db = new ApplicationDbContext();

            Contact contact = db.Contacts.Find(ContactID);

            // Your Account SID from twilio.com/console
            var accountSid = "ACe06162346abf9862f662f27c3fe33c87";
            // Your Auth Token from twilio.com/console
            var authToken = "08c578702041ef1f2578c28443542fd3";

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                to: new PhoneNumber(contact.Phone),
                from: new PhoneNumber("2242796792"),
                body: Text);
        }
        
    }
}