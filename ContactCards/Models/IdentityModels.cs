﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ContactCards.Models
{
       public class ApplicationUser : IdentityUser
    {
        [ForeignKey("OwnerID")]
        public virtual ICollection<Contact> Contacts { get; set; }
        [ForeignKey("SenderID")]
        public virtual ICollection<TextMessage> TextMessages { get; set; }
        [ForeignKey("SenderID")]
        public virtual ICollection<Email> Emails { get; set; }

        [Display(Name = "Name")]
        public override string UserName { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false) {
        }

        public static ApplicationDbContext Create() {
            return new ApplicationDbContext();
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<TextMessage> TextMessages { get; set; }
        public DbSet<Email> Emails { get; set; }
    }
}