﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ContactCards.Models;
using Microsoft.AspNet.Identity;

namespace ContactCards.Controllers
{
    [Authorize]
    public class EmailsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("Contacts/{id}/Emails")]
        public ActionResult Index(int? id)
        {
            if(id == null) {
                return HttpNotFound();
            }
            var emails = db.Emails.ToList().Where(e => e.SenderID == User.Identity.GetUserId() && e.ContactID == id);
            return View(emails.ToList());
        }

        [Route("Contacts/{contactID}/Emails/{id}")]
        public ActionResult Details(int? contactID, int? id)
        {
            if (id == null || contactID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Email email = db.Emails.Find(id);
            if (email == null)
            {
                return HttpNotFound();
            }
            return View(email);
        }

        [Route("Contacts/{id}/Emails/Create")]
        public ActionResult Create(int? id)
        {
            if(id == null) {
                return HttpNotFound();
            }
            ViewBag.ContactID = id;
            ViewBag.SenderID = User.Identity.GetUserId();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Contacts/{id}/Emails/Create")]
        public ActionResult Create([Bind(Include = "Text,Subject,SenderID,ContactID")] Email email)
        {
            if (ModelState.IsValid)
            {
                email.DateSent = DateTime.Now;
                db.Emails.Add(email);
                db.SaveChanges();

                Task.Run(() => email.Send());
                return RedirectToAction("Index");
            }
            return View(email);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
