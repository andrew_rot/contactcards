﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactCards.Models;
using Microsoft.AspNet.Identity;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace ContactCards.Controllers {

    [Authorize]
    public class TextMessagesController : Controller {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Contacts/{id}/Messsages
        [HttpGet]
        [Route("Contacts/{id}/TextMessages")]
        public ActionResult Index(int? id) {
            var textMessages = db.Contacts.Find(id).TextMessages.Where(t => t.SenderID == User.Identity.GetUserId());
            return View(textMessages.ToList());
        }

        [HttpGet]
        [Route("Contacts/{id}/TextMessages/Create")]
        public ActionResult Create(int? id) {
            if(id == null) {
                return new HttpNotFoundResult();
            }

            ViewBag.ContactID = id;
            ViewBag.SenderID = User.Identity.GetUserId();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Contacts/{id}/TextMessages/Create")]
        public ActionResult Create([Bind(Include = "SenderID,Text,TimeSent,ContactID")] TextMessage textMessage) {
            if (ModelState.IsValid) {
                textMessage.TimeSent = DateTime.Now;
                db.TextMessages.Add(textMessage);
                db.SaveChanges();

                textMessage.SendMessage();

                return RedirectToAction("Index");
            }
            return View(textMessage);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
