﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactCards.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Contacts", new { });
        }

        public ActionResult About()
        {
            ViewBag.Message = "About Interactive Contact Cards";

            return View();
        }
    }
}