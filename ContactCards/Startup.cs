﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ContactCards.Startup))]
namespace ContactCards
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
